import React, { Fragment, useState, useEffect } from 'react';
import { useRouteMatch, Link } from "react-router-dom";
// Redux
import { useSelector, useDispatch } from 'react-redux';
//Components
import countries from './countries';

//Icons
import IconLoader from '../../assets/loader.gif';

// Callbacks
import {getCategories} from '../../callbacks';

const SelectCountry = () => {
    const [country, setCountry] = useState('US');
    const match = useRouteMatch();
    const dispatch = useDispatch();
    const categoryItem =  useSelector(state => state.product.categories );
    const isLoadingCategories =  useSelector(state => state.product.loadingCategories );

    useEffect(() => {	
        sessionStorage.setItem('country', country);
        dispatch(getCategories(country));
    }, [dispatch, country]);
    
    const handleChangeCountry  = (country, setCountry, dispatch) => {
        sessionStorage.setItem('country', country);
        dispatch(getCategories(country));
        setCountry(country);
    }
    
    const handleChangeCategory  = (category) => {
        sessionStorage.setItem('category', category);
    }

    return (
        <Fragment>
            <div className='SelectCountry-wrapper'>
                <div className='SelectCountry-cointainer'>
                    <p className='SelectCountry-title'>Seleccióna pais:</p>
                    <div className='SelectCountry-dropdown'>
                        <img 
                            alt='Flag'
                            src={`./flags/${country}.png`} />
                        <span>{ countries[country] }</span>
                    </div>
                    <select 
                        disabled={isLoadingCategories}
                        className='SelectCountry-select SelectCountry-select--flags'
                        onChange={(e) => handleChangeCountry(e.target.value, setCountry, dispatch) } >
                        <option value='US'>Estados Unidos</option>
                        <option value='AU'>Australia</option>
                        <option value='BR'>Brasil</option>
                        <option value='CA'>Canada</option>
                        <option value='CN'>China</option>
                        <option value='FR'>Francia</option>
                        <option value='DE'>Alemania</option>
                        <option value='IN'>India</option>
                        <option value='IT'>Italia</option>
                        <option value='MX'>México</option>
                        <option value='NL'>Paises Bajos</option>
                        <option value='SG'>Singapur</option>
                        <option value='ES'>España</option>
                        <option value='TR'>Turquia</option>
                        <option value='AE'>Emiratos Árabes</option>
                        <option value='GB'>Inglaterra</option>
                        <option value='JP'>Japon</option>
                    </select>
                </div>
                { isLoadingCategories ? 
                    <div className='Main-Loading--Home'>
                        <img 
                            alt='Loader'
                            src={IconLoader} />
                    </div> :
                    categoryItem.error ? 
                    <div>
                        <p className='SelectCountry-error'>
                         No se encontraron categorias, selecciona otra ciudad.
                    </p>
                    </div> :
                    <div className='SelectCountry-cointainer'>
                        <p className='SelectCountry-title'>Seleccióna categoría:</p>
                        <select 
                            disabled={ categoryItem.length === 0 }
                            className='SelectCountry-select'
                            onChange={(e) => handleChangeCategory(e.target.value) }>
                            { categoryItem.length > 0 && categoryItem.map((category) => {
                                return ( 
                                    <option 
                                        key={category.id}
                                        value={category.id}>
                                        {category.name}
                                    </option>
                                )
                            })}
                        </select>
                        <div className='SelectCountry-cointainer'>
                            <Link to={`${match.url}products`}>
                                <button 
                                    className={
                            !isLoadingCategories ? 'SelectCountry-button' : 'SelectCountry-button--disabled'}
                                    disabled={isLoadingCategories}> Ir </button>
                            </Link>
                        </div>
                    </div>
                }
            </div>
        </Fragment>
    );

}

export default SelectCountry;