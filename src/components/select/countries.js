const countries = {
'US': 'Estados Unidos',
'AU': 'Australia',
'BR': 'Brasil',
'CA': 'Canada',
'CN': 'China',
'FR': 'Francia',
'DE': 'Alemania',
'IN': 'India',
'IT': 'Italia',
'MX': 'México',
'NL': 'Paises Bajos',
'SG': 'Singapur',
'ES': 'España',
'TR': 'Turquia',
'AE': 'Emiratos Árabes',
'GB': 'Inglaterra',
'JP': 'Japon',
}

export default countries;