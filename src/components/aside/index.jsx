import React, { Fragment } from 'react';
// Redux
import { useSelector, useDispatch } from 'react-redux';
// Callbacks
import { getAllProducts, getCategories } from '../../callbacks';

const AsideSearchCategory = () => {
    const dispatch = useDispatch();
    const categoryItem =  useSelector(state => state.product.categories );

    const handleCategoryClick = (category) => {
        sessionStorage.setItem('category', category);
        const country = sessionStorage.getItem('country');
		dispatch(getAllProducts(country, category));
    }

    if (categoryItem.length === 0) {
        const country = sessionStorage.getItem('country');
        dispatch(getCategories(country));
    }

    const categoryList = categoryItem.filter((category, index) => { 
        return index > 5 && index < 20 && category;
     });

    return (
        <Fragment>
            <div className='AsideSearchCategory'>
                <span className='AsideSearchCategory-title'>Categorías</span>
                <div className='AsideSearchCategory-container'>
                { categoryList.map((category) => {
                    return (
                        <div 
                            className='AsideSearchCategory-item'
                            id={category.id}
                            key={category.id}
                            onClick={(e) => handleCategoryClick(e.target.id)}> 
                            {category.name}
                        </div>
                    );
                })}
                </div>
            </div>
        </ Fragment>
    )
}

export default AsideSearchCategory;