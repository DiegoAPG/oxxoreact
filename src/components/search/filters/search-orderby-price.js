// Callbacks
import { getSortProducts } from '../../../callbacks';

const SearchOrderByPrice = (dispatch, orderPrice, setOrderPrice) => {
		const sortBy = 'productPrice';
		const orderBy = orderPrice === false ? 'asc' : 'desc';
		
		setOrderPrice(!orderPrice);
		dispatch(getSortProducts({sortBy , orderBy}));
}

export default SearchOrderByPrice;