// Callbacks
import { getAllProducts } from '../../../callbacks';

const SearchFilter = (event, dispatch, searchValue) => {
    event.persist();

    if (event.key === 'Enter' || event.type === 'click') {

        const country = sessionStorage.getItem('country');
        const category = sessionStorage.getItem('category');
        dispatch(getAllProducts(country, category, searchValue));
    }

}

export default SearchFilter;