// Callbacks
import { getSortProducts } from '../../../callbacks';

const SearchOrderByStatus = (dispatch, orderStatus, setOrderStatus) => {
  const sortBy = 'productCount';
  const orderBy = orderStatus === false ? 'asc' : 'desc';
  
  setOrderStatus(!orderStatus);
  dispatch(getSortProducts({sortBy , orderBy}));
}

export default SearchOrderByStatus;