const SearchNotFound = () => {
	return (
		<div className='SearchNotFound'>
			<div className='SearchNotFound-title'>
				<span>No se encontraron resultados para tu búsqueda</span>
			</div>
		</div>	
	);
}

export default SearchNotFound;