import { Fragment } from 'react';

// Icons
import IconArrowLeft from '../../assets/icon-arrow-left.svg';
import IconArrowRight from '../../assets/icon-arrow-right.svg';
// Redux
import { useSelector, useDispatch } from 'react-redux';

import { getAllProducts } from '../../callbacks';

const SearchPagination = () => {
	const dispatch = useDispatch();
	const getAllProductsItems = useSelector(state => state.product.productList );
	const hasNextPage = getAllProductsItems && getAllProductsItems.hasNextPage;
	const hasPrevPage = getAllProductsItems && getAllProductsItems.hasPrevPage;
	const currentPage = getAllProductsItems && getAllProductsItems.page;

  	return (
		<Fragment>
				<div>
					<div className='SearchPagination'>
						{ hasPrevPage && SearchPaginationPrev(dispatch, currentPage) }
						{ hasNextPage && SearchPaginationNext(dispatch, currentPage) }
					</div>
				</div>
		</Fragment>
	)
}

const SearchPaginationPrev = (dispatch, currentPage) => {
	const numberPage = currentPage - 1;
	return (
		<Fragment>
				<span 
					className='SearchPaginationItem'
					onClick={() => SearchPaginationCall(dispatch, numberPage)}>
					<img 
						alt='Pagination left'
						className='SearchPaginationItem-image'
						src={IconArrowLeft} />
				</span>
		</Fragment>
	);
}

const SearchPaginationNext = (dispatch, currentPage) => {
	const numberPage = currentPage + 1;
	return (
		<Fragment>
				<span 
					className='SearchPaginationItem'
					onClick={() => SearchPaginationCall(dispatch, numberPage)}>
					<img 
						alt='Pagination left'
						className='SearchPaginationItem-image'
						src={IconArrowRight} />
				</span>
		</Fragment>
	);
}


const SearchPaginationCall = (dispatch, numberPage) => {
	const country = sessionStorage.getItem('country');
	const category = sessionStorage.getItem('category');
	dispatch(getAllProducts(country, category, null, numberPage))
}

export default SearchPagination;