import React, { Fragment } from 'react';

// components
import SearchList from '../../components/search/search-list-mobile';
import SearchNotFound from '../../components/search/search-not-found';

const ProductsMobile = ({
	products
}) => {
	return (
		<div className='ProductsMobile'>
			{ products && products.length > 0 ? 
				products.map((product, index) => {
				return (
					<Fragment key={index}>
						<SearchList 
							product={product} />
					</Fragment>
				) 
				}) :
				<SearchNotFound />
			}
		</div>
	)
}

export default ProductsMobile;