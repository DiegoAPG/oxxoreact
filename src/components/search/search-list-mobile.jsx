import React, { Fragment } from 'react';

const SearchList = ({
	product: {
		app_sale_price,
		app_sale_price_currency,
		isBestSeller,
		product_detail_url,
		product_main_image_url,
		product_title,
	},
}) => {
	const category = sessionStorage.getItem('category');

	return (
		<Fragment>
			<div className='SearchList'>
				<div className='SearchList-item SearchList-thumbnail-container'>
					<img 
						alt='Thumbnail'
						className='SearchList-thumbnail'
						src={product_main_image_url} />
				</div>
				{ isBestSeller &&
					<div className='SearchList-item SearchList-bestseller'>
					 Más vendido
					</div>
				}
				<div className='SearchList-conteiner-item'>
					<div className='SearchList-item-category'>
						<span className='SearchList-item-category'>{category}</span>
					</div>
					<div className='SearchList-item'>
						<span className='SearchList-item-title'>{`${ product_title.slice(0, 60) }...`}</span>
					</div>
					<div className='SearchList-item'>
						{ app_sale_price ? 
							<div>
								<span className='SearchList-item-description'>{app_sale_price}</span>
								<span className='SearchList-item-description'>{app_sale_price_currency}</span>
							</div> : 
							<div  className='SearchList-item-description'>No disponible</div>
						}
					</div>
					<div className='SearchList-item'>
						<a 
							className='SearchList-item-buttom'
							href={product_detail_url}>Ver más</a>
					</div>
				</div>
		
			</div>
		</Fragment>
	);
}

export default SearchList;