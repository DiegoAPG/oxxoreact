import React from 'react';

// Components
import { SearchButton } from '../../components/search/search-button';
import { SearchButtonDownload } from '../../components/search/search-button-download';

const SearcHeader = ({
	totalProducts
}) => {
	const category = sessionStorage.getItem('category');
    return (
			<div className='SearchHeader-content'>
				<div className='SearchHeader-content--title'>
					<h1 className='Search-title'>
						Productos en <span>{ category } </span>
					</h1>
					{
						totalProducts && 
						<div>Se encontraron {totalProducts} artículos en tu búsqueda</div>
					}
				</div>
				<div  className='SearchHeader-content--buttons'>
					<div className='SearchHeader-content--button'>
						<SearchButton />
					</div>
					<div className='SearchHeader-content--button'>
						<SearchButtonDownload />
					</div>
				</div>
			</div>
    );
}

export default SearcHeader;