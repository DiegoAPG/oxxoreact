import { useState } from 'react';
// Redux
import { useDispatch } from 'react-redux';

// Icons
import IconSearchs from '../../assets/icon-search.svg';

// Filter
import SearchFilter from './filters/search-filter';

export const SearchButton = () => {
  const dispatch = useDispatch();
  const [searchValue, setSearchValue] = useState('');

  return (
    <div className='Search-container'>
        <input
          className='SearchButton-input'
          onKeyDown={(event) => SearchFilter(event, dispatch, searchValue) }
          onChange={e => setSearchValue(e.target.value)}
          placeholder='Has una búsqueda' 
          type='text' />

        <img 
          alt='Search Icon'
          className='Search-icon' 
          onClick={(event) => SearchFilter(event, dispatch, searchValue) }
          src={IconSearchs} />
    </div>
  );
}