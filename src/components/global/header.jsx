import React, { Fragment } from 'react';

const Header = () => {
	const country = sessionStorage.getItem("country");
	const isHomeView = window.location.pathname === '/';
	return (
		<Fragment>
			<div className='Header-wrapper'>
			{ !isHomeView &&
				<div className='Header-currentCountry'>
					<img 
						alt='Flags'
						src={`./flags/${country}.png`} />
				</div>
			}
			</div>
		</Fragment>
	);
}

export default Header;