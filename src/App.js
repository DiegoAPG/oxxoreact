import './styles/main.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

// Components
import Header  from './components/global/header';
import SearchView  from './views/search-view';
import SearchSelectCountryView  from './views/search-select-country-view';

function App() {
  return (
    <div className='Content'>
      {/* <SearchSelectCountryView />
      <SearchView /> */}
      <Router>
        <div>
          <Header />
          <Switch>
            <Route exact path="/">
              <SearchSelectCountryView />
            </Route>
            <Route path="/products">
              <SearchView />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
