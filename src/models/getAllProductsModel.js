const AllProductsModel = {
    fromService(response) {
        return {
            products: response && response.docs,
            hasNextPage: response && response.hasNextPage,
            hasPrevPage: response && response.hasPrevPage,
            page: response && response.page,
            totalPages: response && response.totalPages,
            total_record_count: response && response.total_record_count,
        }
    }
}

export default AllProductsModel;