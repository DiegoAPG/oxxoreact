export const addProduct = () => ({ 
    type: 'ADD_PRODUCT_START',
});

export const product = (product) => ({ 
    type: 'ADD_PRODUCT_SUCCESS',
    product,
});

export const addCategories = () => ({ 
    type: 'ADD_CATEGORIES_START',
});

export const categories = (categories) => ({ 
    type: 'ADD_CATEGORIES_SUCCESS',
    categories,
});