const initialState = {
	productList: [],
	categories: [],
	loadingCategories: false,
	loadingProduct: false,
};

const Product = (state = initialState, action) => {
	switch (action.type) {
		case 'ADD_PRODUCT_START':
			return {
				...state,
				loadingProduct: true,
			}
		case 'ADD_PRODUCT_SUCCESS':
			return {
				...state,
				productList: action.product,
				loadingProduct: false,
			}
		case 'ADD_CATEGORIES_START':
			return {
				...state,
				loadingCategories: true,
			}
		case 'ADD_CATEGORIES_SUCCESS':
			return {
				...state,
				categories: action.categories,
				loadingCategories: false, 
			}
		default:
			return state
	}
}

export default Product;