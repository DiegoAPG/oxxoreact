import baseUrl from '../config/api';
import axios from 'axios';  

import {
  addProduct,
  product,
  addCategories,
  categories,
} from '../redux/actions';

import AllProductsModel from '../models/getAllProductsModel';

const getAllProducts = (country, category, keyword, page) => {
  return function action(dispatch) {
    dispatch(addProduct());
     axios.get(`${baseUrl}/product`,{
      params: {
        country: country,
        categoryID: category,
        keyword: keyword ? keyword : null,
        page: page ? page : null,
      },
      headers: AddHeaders()})
      .then(response => {
        const responseModel = AllProductsModel.fromService(response.data);
        dispatch(product(responseModel));
      })
      .catch(err => {
      });
  }
}

const getCategories = (country) => {
  return function action(dispatch) {
    dispatch(addCategories());
    axios.get(`${baseUrl}/category`,{
      params: {
        country: country,
      },
      headers: AddHeaders()})
      .then(data => {
        dispatch(categories(data.data));
      })
      .catch(err => {
        console.log(err);
      });
  }
}


const AddHeaders = () => {
  return {
    // "x-rapidapi-key": "5e675bcf21msh3b13e68371e8e67p1f778djsn8f41bcf97607",
    // "x-rapidapi-host": "amazon24.p.rapidapi.com",
    'x-rapidapi-key': '6374e89aa1mshfd084d22ee70414p170e19jsnf0174cd69c86',
    'x-rapidapi-host': 'amazon24.p.rapidapi.com',
    "useQueryString": true
  }
}

export {
  getAllProducts,
  getCategories
};