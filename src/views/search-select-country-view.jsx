import React, { Fragment } from 'react';

// Components
import SelectCountry from '../components/select/select-country';

const SearchView = () => {
	return (
		<Fragment>
			<div className='Content-wrapper'>
				<div className='Main-wrapper'>
					<div className='Main-background' />
					<div className='Search-content'>
						<SelectCountry  />	
					</div>
				</div>
			</div>
		</Fragment>
	);
}

export default SearchView;