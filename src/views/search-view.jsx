import React, { Fragment, useEffect, useState} from 'react';
// Redux
import { useSelector, useDispatch } from 'react-redux';

// Components
import SearcHeader from '../components/search/search-header';
import SearchPagination from '../components/search/search-pagination';
import SearchProducts from '../components/search/search-products';
import AsideSearchCategory from '../components/aside';

// Callbacks
import { getAllProducts } from '../callbacks';

//Icons
import IconLoader from '../assets/loader.gif';

const SearchView = () => {
	const [currentPage, setCurrentPage] = useState(1);

	const getAllProductsItems =  useSelector(state => state.product.productList );
	const totalProducts =  getAllProductsItems && getAllProductsItems.total_record_count;
	const isLoadingProduct =  useSelector(state => state.product.loadingProduct );

	const products = getAllProductsItems && getAllProductsItems.products;
	const dispatch = useDispatch();

	useEffect(() => {	
		const country = sessionStorage.getItem('country');
		const category = sessionStorage.getItem('category');
		dispatch(getAllProducts(country, category));
	}, [dispatch]);

	return (
		<Fragment>
			<div className='Content-wrapper'>
				<div className='Main-wrapper'>
					<div className='Main-background' />
					<div className='Search-content'>
						{ isLoadingProduct ?
							<div className='Main-Loading'>
								<img 
									alt='Loader'
									src={IconLoader} />
							</div> :
							<div className='Search-content-product'>
								<AsideSearchCategory />
								<div className='Search-content-product-items'>
									<SearcHeader 
										totalProducts={totalProducts} />
									<SearchProducts 
										products={products} />
									{ products && products.length > 0 &&
										<SearchPagination 
											currentPage={currentPage}
											setCurrentPage={setCurrentPage} />
									}	
								</div>
							</div>
						}
						</div>
					</div>
			</div>
		</Fragment>
	);
}

export default SearchView;